#!/usr/bin/bash
# Merge patches from RPM package to Git

set -efu

name="${name:-rpm}"
source="${source:-/mnt/dev/rosa-pkgs/rpm/}"
source_spec="${source}/${name}.spec"
target="${target:-/mnt/dev/sources/rpm5/rpm5/}"

dir0="$PWD"
cd "$target"
if ! grep -q "\.rej" .gitignore; then
	echo "*.rej" >> .gitignore
	echo "*.orig" >> .gitignore
	git add .gitignore
	git commit -m "Update .gitignore before patching" --author="Patch Merge Script <noreply@nixtux.ru>"
fi

while read -r line
do
	n1="$(echo "$line" | awk '{print $1}' | sed -e 's,%patch,,g')"
	n0="${n0:-${n1}}"
	patch_filename="$(cat "$source_spec" | grep "^Patch${n1}:" | awk '{print $NF}')"
	[ -f "${source}/${patch_filename}" ]
	if grep '^From: ' "${source}/${patch_filename}"
		then
			git am "${source}/${patch_filename}"
		else
			patch --no-backup-if-mismatch -p1 < "${source}/${patch_filename}"
			#find . -name "*.rej" -o -name "*.orig" -delete
			commit_msg="$(sed -n "/^Patch${n0}/,/^Patch${n1}/p" "${source_spec}" | tac | tail -n +2 | sed '/^Patch/q' | head -n +2 | sed -e 's,^# ,,g' | grep -vE '^Patch.*:' | tac)"
			commit_msg_file="$(mktemp)"
			echo -e "${patch_filename}" " (Patch${n1})  " | sed -e 's,.patch ,,g' > "$commit_msg_file"
			#echo "" > "$commit_msg_file"
			echo "$commit_msg" >> "$commit_msg_file"
			git add .
			git commit -m "$(cat "$commit_msg_file")" --author="Patch Merge Script <noreply@nixtux.ru>"
			rm -f "$commit_msg_file"
	fi
	# remember previous number
	n0="$n1"
done < <(cat "${source_spec}" | grep '^\%patch')
