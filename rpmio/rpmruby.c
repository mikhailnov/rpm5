#include "system.h"
#include <argv.h>

#if defined(MODULE_EMBED)

/* Make sure Ruby's fun stuff has its own xmalloc & Co functions available */
#undef xmalloc
#undef xcalloc
#undef xrealloc

/* XXX avoid conflicting definitions in ruby.h */
#define HAVE_SETPROCTITLE 1
 
#include <ruby.h>
#undef WITH_RUBYEMBED

#endif

#if defined(WITH_RUBYEMBED)
#include <dlfcn.h>
#include <rpmlog.h>
#endif

#define _RPMRUBY_INTERNAL 1
#include "rpmruby.h"

#include "debug.h"

/*@unchecked@*/
int _rpmruby_debug = 0;

/*@unchecked@*/ /*@relnull@*/
rpmruby _rpmrubyI = NULL;

/**
 * Finalizes a Ruby interpreter instance/pool item
 */
static void rpmrubyFini(void *_ruby)
        /*@globals fileSystem @*/
        /*@modifies *_ruby, fileSystem @*/
{
    rpmruby ruby = (rpmruby) _ruby;

#   if defined(MODULE_EMBED)
    ruby_cleanup(0); 
#endif
    ruby->I = NULL;
}

/**
* The pool of Ruby interpreter instances
* @see rpmioPool
*/
/*@unchecked@*/ /*@only@*/ /*@null@*/
rpmioPool _rpmrubyPool;

/**
 * Returns the current rpmio pool responsible for Ruby interpreter instances
 *
 * This is a wrapper function that returns the current rpmio pool responsible
 * for embedded Ruby interpreters. It creates and initializes a new pool when
 * there is no current pool.
 *
 * @return The current pool
 * @see rpmioNewPool
 */
static rpmruby rpmrubyGetPool(/*@null@*/ rpmioPool pool)
        /*@globals _rpmrubyPool, fileSystem @*/
        /*@modifies pool, _rpmrubyPool, fileSystem @*/
{
    rpmruby ruby;

    if (_rpmrubyPool == NULL) {
        _rpmrubyPool = rpmioNewPool("ruby", sizeof(*ruby), -1, _rpmruby_debug,
            NULL, NULL, rpmrubyFini);
        pool = _rpmrubyPool;
    } 

    return (rpmruby) rpmioGetPool(pool, sizeof(*ruby));
}

#if defined(WITH_RUBYEMBED)
/** Initializes Ruby's StringIO for storing output in a string. */
/*@unchecked@*/
static const char * rpmrubyInitStringIO = "\
require 'stringio'\n\
$stdout = StringIO.new($result, \"w+\")\n\
";
#endif

static rpmruby rpmrubyI()
        /*@globals _rpmrubyI @*/
        /*@modifies _rpmrubyI @*/
{
    if (_rpmrubyI == NULL)
        _rpmrubyI = rpmrubyNew(NULL, 0);
    return _rpmrubyI;
}

#if defined(WITH_RUBYEMBED)
static void loadModule(void) {
    const char librpmruby[] = "rpmruby.so";
    void *h;

    h = dlopen (librpmruby, RTLD_NOW|RTLD_GLOBAL);
    if (!h)
    {
	rpmlog(RPMLOG_WARNING, D_("Unable to open \"%s\" (%s), "
		    "embedded ruby will not be available\n"),
		librpmruby, dlerror());
    }
    else if(!((rpmrubyNew_p = dlsym(h, "rpmrubyNew"))
		&& (rpmrubyRun_p = dlsym(h, "rpmrubyRun")))) {
	rpmlog(RPMLOG_WARNING, D_("Opened library \"%s\" is incompatible (%s), "
		    "embedded ruby will not be available\n"),
		librpmruby, dlerror());
	if (dlclose (h))
	    rpmlog(RPMLOG_WARNING, "Error closing library \"%s\": %s", librpmruby,
		    dlerror());
    } else
	dlopened = 1;
}
#endif

rpmruby rpmrubyNew(char **av, uint32_t flags)
{
#if defined(WITH_RUBYEMBED)
    if (!dlopened) loadModule();
    if (dlopened) return rpmrubyNew_p(av, flags);
#endif

    static const char *_av[] = { "rpmruby", NULL };
    
    /* XXX FIXME: recurse like every other embedded interpreter. */
    if (_rpmrubyI)
        return _rpmrubyI;

    rpmruby ruby =  (flags & 0x80000000)
	? rpmrubyI() : rpmrubyGetPool(_rpmrubyPool);

    if (av == NULL)
        av = (char **) _av;

#   if defined(MODULE_EMBED)
    RUBY_INIT_STACK;
    ruby_init();
    ruby_init_loadpath();

    ruby_script((char *)av[0]);
    rb_gv_set("$result", rb_str_new2(""));
    (void) rpmrubyRun(ruby, rpmrubyInitStringIO, NULL);
#endif

    return rpmrubyLink(ruby);
}

#if defined(WITH_RUBYEMBED)
static int dlopened = 0;
static rpmruby (*rpmrubyNew_p) (char ** av, uint32_t flags);
static rpmRC (*rpmrubyRun_p) (rpmruby ruby, const char * str, const char ** resultp);
#endif

rpmRC rpmrubyRun(rpmruby ruby, const char *str, const char **resultp)
{
#if defined(WITH_RUBYEMBED)
    if (dlopened) return rpmrubyRun_p(ruby, str, resultp);
#endif

    rpmRC rc = RPMRC_FAIL;

if (_rpmruby_debug)
fprintf(stderr, "==> %s(%p,%s,%p)\n", __FUNCTION__, ruby, str, resultp);

    if (ruby == NULL)
        ruby = rpmrubyI();

#if defined(MODULE_EMBED)
    if (str) {
	int state = -1;
	ruby->state = rb_eval_string_protect(str, &state);

	/* Check whether the evaluation was successful or not */

	if (state == 0) {
	    rc = RPMRC_OK;
	    if (resultp)
		*resultp = RSTRING_PTR(rb_gv_get("$result"));
	}
    }
#endif

    return rc;
}

