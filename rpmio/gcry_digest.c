/** \ingroup signature
 * \file rpmio/gcry_digest.c
 */

#if defined(WITH_GCRYPT)

#include <gcrypt.h>

static int
rpmgcDigestReset (void * param)
{
  gcry_md_reset (*(gcry_md_hd_t*)param);
  return 0;
}
static int
rpmgcDigestUpdate (void * param, const byte * _data, size_t _len)
{
  gcry_md_write (*(gcry_md_hd_t*)param, (const void *)_data, _len);
  return 0;
}

static int rpmgcDigestGOST3411_94 (void * param, byte * digest)
{
  unsigned char * h = gcry_md_read (*(gcry_md_hd_t*)param, GCRY_MD_GOSTR3411_CP);
  unsigned int diglen = gcry_md_get_algo_dlen(GCRY_MD_GOSTR3411_CP);
  memcpy(digest, h, diglen);
  gcry_md_close(*(gcry_md_hd_t*)param);
  return 0;
}
static int
rpmgcDigestSTRIBOG256 (void * param, byte * digest)
{
  unsigned char * h = gcry_md_read (*(gcry_md_hd_t*)param, GCRY_MD_STRIBOG256);
  unsigned int diglen = gcry_md_get_algo_dlen(GCRY_MD_STRIBOG256);
  memcpy(digest, h, diglen);
  gcry_md_close(*(gcry_md_hd_t*)param);
  return 0;
}
static int
rpmgcDigestSTRIBOG512 (void * param, byte * digest)
{
  unsigned char * h = gcry_md_read (*(gcry_md_hd_t*)param, GCRY_MD_STRIBOG512);
  unsigned int diglen = gcry_md_get_algo_dlen(GCRY_MD_STRIBOG512);
  memcpy(digest, h, diglen);
  gcry_md_close(*(gcry_md_hd_t*)param);
  return 0;
}

#endif
